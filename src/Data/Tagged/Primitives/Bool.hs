module Data.Tagged.Primitives.Bool (
  Bool,
  (&&),
  (||),
  not,
  otherwise,
  bool
) where

import qualified Data.Bool as Native

import Data.Bits
import Data.Data
import GHC.Generics
import Data.Ix
import Foreign.Storable
import Prelude (Bounded, Enum, Eq, Ord, Read, Show)

newtype Bool a =
  Bool Native.Bool deriving (
    Bounded, Enum, Eq, Data, Ord, Read, Show,
    Ix, Generic, Typeable, Bits, FiniteBits, Storable
  )

(&&) :: Bool a -> Bool a -> Bool a
(Bool a) && (Bool b) = Bool (a Native.&& b)

(||) :: Bool a -> Bool a -> Bool a
(Bool a) || (Bool b) = Bool (a Native.|| b)

not :: Bool a -> Bool a
not (Bool a) = Bool (Native.not a)

otherwise :: Bool a
otherwise = Bool Native.True

bool :: a -> a -> Bool a -> a
bool a b (Bool p) = if p then a else b
