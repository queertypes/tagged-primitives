module Data.Tagged.Primitive.ByteString (
) where

import qualified Data.ByteString as Native

import Control.DeepSeq
import Data.Bits
import Data.Data
import Data.Monoid
import Data.String
import Data.Ix
import Data.Word (Word8)
import Foreign.Storable
import GHC.Generics
import Prelude (Eq, Ord, Read, Show, (.))

data Latin1
data Utf8
data Utf16LE
data Utf16BE
data Utf32LE
data Utf32BE

newtype ByteString enc a = ByteString Native.ByteString deriving (
    Eq, Data, Ord, Read, Show, IsString, Monoid, NFData, Typeable
  )

empty :: ByteString enc a
empty = ByteString Native.empty

singleton :: Word8 -> ByteString enc a
singleton = ByteString . Native.singleton

pack :: [Word8] -> ByteString enc a
pack = ByteString . Native.pack

unpack :: ByteString enc a -> [Word8]
unpack (ByteString b) = Native.unpack b

cons :: Word8 -> ByteString enc a -> ByteString enc a
cons w (ByteString b) = ByteString (Native.cons w b)

snoc :: ByteString enc a -> Word8 -> ByteString enc a
snoc (ByteString b) w = ByteString (Native.snoc b w)

append :: ByteString enc a -> ByteString enc a -> ByteString enc a
append (ByteString a) (ByteString b) = ByteString (Native.append a b)

head :: ByteString enc a -> Word8
head (ByteString b) = Native.head b
