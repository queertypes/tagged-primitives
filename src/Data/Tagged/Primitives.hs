module Data.Tagged.Primitives (
  Bool,
  ByteString,
  Char,
  Double,
  Float,
  Integer,
  Int,
  Int8,
  Int16,
  Int32,
  Int64,
  String,
  Text,
  Word,
  Word8,
  Word16,
  Word32,
  Word64
) where

import Data.Tagged.Primitives.Bool
import Data.Tagged.Primitives.ByteString
import Data.Tagged.Primitives.Char
import Data.Tagged.Primitives.Double
import Data.Tagged.Primitives.Float
import Data.Tagged.Primitives.Integer
import Data.Tagged.Primitives.Int
import Data.Tagged.Primitives.String
import Data.Tagged.Primitives.Text
import Data.Tagged.Primitives.Word
